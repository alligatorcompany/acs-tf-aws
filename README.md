# AWS Infrastruktur für ACS PoC

Die AWS Infrastruktur für den PoC sieht wie folgt aus:

![AWS infrastructure for PoC](awspoc.drawio.svg "AWS infrastructure for PoC")

Zugänge aus dem Internet in die Infrastruktur erfolgen im public subnet und werden über die Variable dev_public_cidr entsprechend eingeschränkt.

Mit folgenden Schritten kann die Infrastruktur in AWS angelegt werden:
1. Instanzgrößen einstellen. Per default sind Instanzgrößen von 't2.micro' eingestellt, um nicht versehentlich grosse Kosten zu generieren. Diese sollten entsprechend den Empfehlungen eingetragen werden, damit das Arbeiten auch fluessig ablaufen kann.
2. Festlegung der Ziel VPC id
3. Festlegung der cidr Blöcke / Netzwerkbereiche, von denen auf die Entwicklerdesktops zugegriffen werden kann.
4. In den Skripten wird defaultmässig der Profilname 'acs-aws' mit der Zone eu-central-1 verwendet. Dies kann man mit dem aws-cli entsprechend anlegen. Anschließend kann mit terraform die Infrastruktur angelegt werden.

Beispielskript:
```
aws configure --profile acs-aws
export AWS_PROFILE=acs-aws
terraform init
terraform apply --auto-approve
```

Der initiale Zugriffsschluessel wird mitgenereirt und in den EC2 Instanzen autorisiert. Inital können diese dann durch andere Schlüssel ersetzt werden.

# Entwicklermaschine
Die Entwicklermaschine wird über das script template/awslinux_acswb.sh gemäß der ACS workbench ausgestattet, so dass Stages (k3s Cluster über k3d.io) und Disposable Envs angelegt werden können.

Die Entwicklungsmaschinen (gemäss den Variablen zur Entwicklerdesktops) haben einen tigervnc service definiert, der auf Port 5901 über einen ssh Tunnel aufgerufen werden kann: ssh -L 5901:localhost:5901 -i ec2Key.pem ec2-user@<ip>

https://tigervnc.org/

Empfehlung für die EC2 Instanzparameter der Entwicklerdesktops:
- Amazon Linux 2
- 16GB RAM und 4 CPU core


Als Sever wird standardmäßig ein dockerhost gestartet, auf dem mittels k3d.io eine Stage (k3s cluster) angelegt wird. Hier laufen Gitlab, Disposable envs und Argo Workflows.

Empfehlung für die dockerhost
- Amazon Linux2
- docker 20.10.x
- 8GB RAM und 20GB lokaler Speicher (SSD) für gitlab
- 12GB RAM und 50GB lokaler Speicher (SSD) pro disposable env

# Variablen

## Entwicklerdesktops
Eine Map mit der Anzahlder gewünschten Entwicklerdesktops. Es kann jeweils 
- der Key wird als tag Name angelegt
- die Größe als instance_type angegeben werden
variable "dev_machines" {
  description = "Map of dev machine configurations"
  type        = map(any)

  default = {
    dev_machine_1 = {
      instance_type = "t2.micro"
    },
    dev_machine_2 = {
      instance_type = "t2.micro"
    }
  }
}

## Server als Dockerhost
Für den Dockerhost auf dem per k3d.io ein k3s cluster angelegt wird, wenn eine Stage angelegt wird. Hier kann die Größe über den instance_type definiert werden.
Beispiel:
variable "dev_dockerhost_insttype" {
  description = "Instance type for dockerhost - should have 18gb RAM per disposable env"
  default     = "t2.micro"
}

## Public Internet Access

Die Variable "dev_public_cidr" enthält eine Liste der cidr-Blöcke, die zuf die Entwicklerdesktops zugreifen sollen:
Beispiel:

```
variable "dev_public_cidr" {
  description = "Public access cidr blocks"
  type        = list(any)
  default     = ["172.105.130.191/32"]
}
```

## VPC
Ein passendes /16er Netz als VPC sollte bereits bereitstehen. Die Variable aws_vpc_id sollte die zugehörige id enthalten:
Beispiel:

```
variable "aws_vpc_id" {
  description = "existing aws vpc e.g. 10.0.0.0/16 cidr"
  default     = "vpc-0cfff2eeda21803f1"
}
```

## Public und Private Subnetze
### CIDR

```
variable "aws_public_subnet_azone" {
  description = "public subnet availability zone"
  default     = "euc1-az1"
}
```

### Availability Zone

```
variable "aws_public_subnet_cidr" {
  description = "cidr for public subnet in vpc"
  default     = "10.0.0.0/24"
}
```


# Links
- https://alligatorcompany.gitlab.io/acs-docs
- https://aws.amazon.com/premiumsupport/knowledge-center/ec2-linux-2-install-gui/
- https://medium.com/@kuldeep.rajpurohit/vpc-with-public-and-private-subnet-nat-on-aws-using-terraform-85a18d17c95e
- https://github.com/krrajpurohit/terra-infra/tree/master/private-public-subnet-with-NAT
