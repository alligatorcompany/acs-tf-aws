terraform {
  backend "s3" {
    bucket         = "acs-terraform-dev"
    key            = "terraform.tfstate"
    region         = "eu-central-1"
    encrypt        = true
    dynamodb_table = "terraform-lock"
  }
}
