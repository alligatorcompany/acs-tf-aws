# this will create a key with RSA algorithm with 4096 rsa bits
resource "tls_private_key" "private_key" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

# this resource will create a key pair using above private key
resource "aws_key_pair" "key_pair" {
  key_name   = var.key_name
  public_key = tls_private_key.private_key.public_key_openssh

  depends_on = [tls_private_key.private_key]
}

# this resource will save the private key at our specified path.
resource "local_file" "saveKey" {
  content  = tls_private_key.private_key.private_key_pem
  filename = "${var.key_name}.pem"
}

# dev host security group
resource "aws_security_group" "sg_dev_host" {
  name        = "sg dev host"
  description = "dev host security group"
  vpc_id      = var.aws_vpc_id

  ingress {
    description = "allow SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = var.dev_public_cidr
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# dev host ec2 instance
resource "aws_instance" "dev_host" {
  depends_on = [
    aws_security_group.sg_dev_host, aws_key_pair.key_pair
  ]
  for_each               = var.dev_machines
  ami                    = "ami-05ff5eaef6149df49"
  instance_type          = each.value.instance_type
  key_name               = var.key_name
  vpc_security_group_ids = [aws_security_group.sg_dev_host.id]
  subnet_id              = aws_subnet.public_subnet.id

  user_data = file("template/awslinux_acswb.sh")

  tags = {
    Name = "devhost-${each.key}"
  }
}

resource "aws_security_group" "sg_dockerhost" {
  name        = "sg dockerhost"
  description = "dockerhost security group"
  vpc_id      = var.aws_vpc_id

  ingress {
    description     = "allow SSH"
    from_port       = 22
    to_port         = 22
    protocol        = "tcp"
    security_groups = [aws_security_group.sg_dev_host.id]
  }
  ingress {
    description     = "allow HTTPS"
    from_port       = 443
    to_port         = 443
    protocol        = "tcp"
    security_groups = [aws_security_group.sg_dev_host.id]
  }
  ingress {
    description     = "allow kubeapi"
    from_port       = 6443
    to_port         = 6443
    protocol        = "tcp"
    security_groups = [aws_security_group.sg_dev_host.id]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# dev dockerhost
resource "aws_instance" "dev_dockerhost" {
  depends_on = [
    aws_security_group.sg_dockerhost,
    aws_nat_gateway.nat_gateway,
    aws_route_table_association.associate_routetable_to_private_subnet,
  ]
  ami                    = "ami-05ff5eaef6149df49"
  instance_type          = var.dev_dockerhost_insttype
  key_name               = var.key_name
  vpc_security_group_ids = [aws_security_group.sg_dockerhost.id]
  subnet_id              = aws_subnet.private_subnet.id
  user_data              = file("template/awslinux_docker.sh")
  tags = {
    Name = "dev-dockerhost"
  }
}
