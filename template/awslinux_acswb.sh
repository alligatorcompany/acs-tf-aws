#!/bin/bash

yum update -y

amazon-linux-extras install mate-desktop1.x -y
bash -c 'echo PREFERRED=/usr/bin/mate-session > /etc/sysconfig/desktop'
yum install tigervnc-server -y
mkdir /etc/tigervnc
bash -c 'echo localhost > /etc/tigervnc/vncserver-config-mandatory'
cp /lib/systemd/system/vncserver@.service /etc/systemd/system/vncserver@.service
sed -i 's/<USER>/ec2-user/' /etc/systemd/system/vncserver@.service
systemctl daemon-reload
systemctl enable vncserver@:1
su - ec2-user -c "mkdir ~/.vnc && echo SecurityTypes=None >> ~/.vnc/config && sudo systemctl restart vncserver@:1"
amazon-linux-extras install epel java-openjdk11
wget --quiet https://dbeaver.io/files/dbeaver-ce-latest-stable.x86_64.rpm -O dbeaver.rpm
yum install dbeaver.rpm  -y
yum install chromium asciidoctor parallel unixODBC jq htop unzip -y
pip3 install csvkit dbt-core==1.2.2 dbt-exasol sqlfluff
wget --quiet 'https://code.visualstudio.com/sha/download?build=stable&os=linux-rpm-x64' -O vscode.rpm
yum install vscode.rpm -y
su - ec2-user -c "code --install-extension asciidoctor.asciidoctor-vscode --force"
su - ec2-user -c "code --install-extension bastienboutonnet.vscode-dbt --force"
su - ec2-user -c "code --install-extension bierner.markdown-mermaid --force"
su - ec2-user -c "code --install-extension dorzey.vscode-sqlfluff --force"
su - ec2-user -c "code --install-extension gitpod.gitpod-desktop --force"
su - ec2-user -c "code --install-extension gitpod.gitpod-remote-ssh --force"
su - ec2-user -c "code --install-extension gitpod.gitpod-theme --force"
su - ec2-user -c "code --install-extension hashicorp.terraform --force"
su - ec2-user -c "code --install-extension hediet.vscode-drawio --force"
su - ec2-user -c "code --install-extension henriblancke.vscode-dbt-formatter --force"
su - ec2-user -c "code --install-extension innoverio.vscode-dbt-power-user --force"
su - ec2-user -c "code --install-extension mechatroner.rainbow-csv --force"
su - ec2-user -c "code --install-extension ms-python.python --force"
su - ec2-user -c "code --install-extension ms-toolsai.jupyter --force"
su - ec2-user -c "code --install-extension ms-toolsai.jupyter-keymap --force"
su - ec2-user -c "code --install-extension ms-toolsai.jupyter-renderers --force"
su - ec2-user -c "code --install-extension ms-python.vscode-pylance --force"
su - ec2-user -c "code --install-extension ms-toolsai.vscode-jupyter-cell-tags --force"
su - ec2-user -c "code --install-extension ms-toolsai.vscode-jupyter-slideshow --force"
su - ec2-user -c "code --install-extension ms-vscode-remote.remote-containers --force"
su - ec2-user -c "code --install-extension ms-vscode-remote.remote-ssh --force"
su - ec2-user -c "code --install-extension ms-vscode-remote.remote-ssh-edit --force"
su - ec2-user -c "code --install-extension sainnhe.gruvbox-material --force"
su - ec2-user -c "code --install-extension samuelcolvin.jinjahtml --force"
su - ec2-user -c "code --install-extension yzhang.markdown-all-in-one --force"
yum install git -y
wget --quiet https://github.com/jesseduffield/lazygit/releases/download/v0.35/lazygit_0.35_Linux_x86_64.tar.gz
tar xfz lazygit_0.35_Linux_x86_64.tar.gz -C /usr/local/bin/ lazygit
chmod +x /usr/local/bin/lazygit
rm lazygit_0.35_Linux_x86_64.tar.gz

curl -LO https://github.com/k3d-io/k3d/releases/download/v5.4.1/k3d-linux-amd64 
mv k3d-linux-amd64 /bin/k3d && chmod +x /bin/k3d

curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.23.5/bin/linux/amd64/kubectl
mv kubectl /bin/ && chmod +x /bin/kubectl

curl -LO https://github.com/peak/s5cmd/releases/download/v1.4.0/s5cmd_1.4.0_Linux-64bit.tar.gz
tar xfz s5cmd*gz -C /bin/ s5cmd && rm -f *gz && chmod +x /bin/s5cmd

curl -LO https://github.com/gohugoio/hugo/releases/download/v0.96.0/hugo_extended_0.96.0_Linux-64bit.tar.gz
tar xfz hugo*gz -C /bin/ hugo && rm -f *gz && chmod +x /bin/hugo

curl -LO https://github.com/argoproj/argo-workflows/releases/download/v3.3.1/argo-linux-amd64.gz
gunzip argo* && mv argo* /bin/argo  && chmod +x /bin/argo

curl -LO https://download.docker.com/linux/static/stable/x86_64/docker-20.10.14.tgz
tar xfz docker-*.tgz && mv docker/docker /bin/docker && rm -f *.tgz && chmod +x /bin/docker

curl -LO https://github.com/derailed/k9s/releases/download/v0.25.18/k9s_Linux_x86_64.tar.gz
tar xfz k9s*.tar.gz -C /bin/ k9s && rm -f *.tar.gz && chmod +x /bin/k9s

curl -LO https://github.com/bitnami-labs/sealed-secrets/releases/download/v0.17.3/kubeseal-0.17.3-linux-amd64.tar.gz
tar xfz kube*.tar.gz -C /bin/ kubeseal && chmod +x /bin/kubeseal && rm -f kubeseal*.tar.gz

curl -s "https://raw.githubusercontent.com/kubernetes-sigs/kustomize/master/hack/install_kustomize.sh"  | bash 
mv kustomize /bin/kustomize && chmod +x /bin/kustomize

curl -LO https://releases.hashicorp.com/terraform/1.1.7/terraform_1.1.7_linux_amd64.zip && \
unzip terraform*.zip && mv terraform /bin/terraform && rm terraform*zip && chmod +x /bin/terraform

curl -LO https://github.com/ahmetb/kubectx/releases/download/v0.9.4/kubectx_v0.9.4_linux_x86_64.tar.gz
curl -LO https://github.com/ahmetb/kubectx/releases/download/v0.9.4/kubens_v0.9.4_linux_x86_64.tar.gz
curl -LO https://github.com/kvaps/kubectl-build/raw/master/kubectl-build
tar xfz kubens*tar.gz && tar xfz kubectx*tar.gz && rm *.tar.gz && chmod 0755 kube* && mv kube* /bin/

curl -LO https://github.com/okteto/okteto/releases/download/2.0.2/okteto-Linux-x86_64 
chmod +x okteto-Linux-x86_64 && mv okteto-Linux-x86_64 /bin/okteto && chmod +x /bin/okteto

wget --quiet https://github.com/BurntSushi/xsv/releases/download/0.13.0/xsv-0.13.0-x86_64-unknown-linux-musl.tar.gz
tar xfz xsv-*.tar.gz -C /bin/ xsv && chmod +x /bin/xsv && rm -f xsv*.tar.gz

wget --quiet https://www.exasol.com/support/secure/attachment/198356/EXASOL_ODBC-7.1.7.tar.gz -O exasol_odbc.tar.gz
wget --quiet https://www.exasol.com/support/secure/attachment/198012/EXAplus-7.1.7.tar.gz -O exaplus.tar.gz
tar zxf exasol_odbc.tar.gz \
  && mv EXASolution_ODBC-*/lib/linux/x86_64/ odbc \
  && tar zxf exaplus.tar.gz \
  && mv EXAplus-*/ exaplus \
  && rm -rf exaplus/doc
mv exaplus /opt/
mkdir /opt/exasol && mv odbc /opt/exasol/
rm -f *tar.gz
cat <<EOT >> /etc/odbcinst.ini

[Exasol]
Description = ODBC for Exasol
Driver      = /opt/exasol/odbc/libexaodbc-uo2214lv2.so
EOT

systemctl restart vncserver@:1
