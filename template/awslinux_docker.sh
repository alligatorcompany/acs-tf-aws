#!/bin/bash

yum update -y
yum install -y docker
usermod -a -G docker ec2-user
newgrp docker
systemctl enable docker.service
systemctl start docker.service
