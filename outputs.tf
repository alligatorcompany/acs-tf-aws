output "dev_host_ip" {
  description = "public ip of dev host"
  value = tomap({
    for k, inst in aws_instance.dev_host : k => inst.public_ip
  })
}

output "dev_dockerhost_ip" {
  description = "private ip of dev dockerhost"
  value       = aws_instance.dev_dockerhost.private_ip
}
