variable "key_name" {
  default = "ec2Key" # if we keep default blank it will ask for a value when we execute terraform apply
}

variable "aws_vpc_id" {
  description = "existing aws vpc e.g. 10.0.0.0/16 cidr"
  default     = "vpc-0cfff2eeda21803f1"
}
variable "aws_public_subnet_azone" {
  description = "public subnet availability zone"
  default     = "euc1-az1"
}
variable "aws_private_subnet_azone" {
  description = "private subnet availability zone"
  default     = "euc1-az1"
}
variable "aws_public_subnet_cidr" {
  description = "cidr for public subnet in vpc"
  default     = "10.0.0.0/24"
}
variable "aws_private_subnet_cidr" {
  description = "cidr for private subnet in vpc"
  default     = "10.0.1.0/24"
}
variable "dev_machines" {
  description = "Map of dev machine configurations"
  type        = map(any)

  default = {
    dev_machine_1 = {
      instance_type = "t2.micro"
    },
    dev_machine_2 = {
      instance_type = "t2.micro"
    }
  }
}

variable "dev_dockerhost_insttype" {
  description = "Instance type for dockerhost - should have 18gb RAM per disposable env"
  default     = "t2.micro"
}

variable "dev_public_cidr" {
  description = "Public access cidr blocks"
  type        = list(any)
  default     = ["172.105.130.191/32"]
}
